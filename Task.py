# Sample code from https://www.redblobgames.com/pathfinding/a-star/
# Copyright 2014 Red Blob Games <redblobgames@gmail.com>
#
# Feel free to use this code in your own projects, including commercial projects
# License: Apache v2.0 <http://www.apache.org/licenses/LICENSE-2.0.html>

from queue import PriorityQueue
from Map import Map_Obj
from typing import Iterator, Tuple

map_obj = Map_Obj(task=4)

def heuristic(a, b) -> float:
    (x1, y1) = a
    (x2, y2) = b
    return abs(x1 - x2) + abs(y1 - y2)

def neighbors(node):
    dirs = [[1, 0], [0, 1], [-1, 0], [0, -1]]
    result = []
    for dir in dirs:
        neighbor = [node[0] + dir[0], node[1] + dir[1]]
        if map_obj.get_cell_value(neighbor) != -1:
            result.append(neighbor)
    return result



def a_star_search(start, goal):
    frontier = PriorityQueue()
    frontier.put(start,0)
    came_from = {}
    cost_so_far = {}
    came_from[tuple(start)] = None
    cost_so_far[tuple(start)] = 0
    
    while not frontier.empty():
        current = frontier.get()
        
        for next in neighbors(current):
            new_cost = cost_so_far[tuple(current)] + map_obj.get_cell_value(next)                

            if tuple(next) not in cost_so_far or new_cost < cost_so_far[tuple(next)]:
                cost_so_far[tuple(next)] = new_cost
                priority = new_cost + heuristic(next, goal)
                frontier.put(next, priority)
                came_from[tuple(next)] = current
            
    return came_from, cost_so_far

# thanks to @m1sp <Jaiden Mispy> for this simpler version of
# reconstruct_path that doesn't have duplicate entries

def reconstruct_path(came_from,start, goal):
    current = goal
    path = []
    if tuple(goal) not in came_from: # no path was found
        return []
    while current != start:
        path.append(current)
        map_obj.set_cell_value(current,5)
        current = came_from[tuple(current)]
    path.append(start) # optional
    path.reverse() # optional
    return path



came_from, cost_so_far = a_star_search(map_obj.get_start_pos(),map_obj.get_goal_pos())
print(reconstruct_path(came_from, map_obj.get_start_pos(), map_obj.get_goal_pos()))
map_obj.show_map()