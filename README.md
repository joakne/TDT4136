
A star search function "def a_star_search":   

Creates a frontier queue by using the built in PriorityQueue() function. This helps us minimizing the amount of code needed to keep track and sort a queue so that the best possible node with the lowest heuristics comes to the top of the stack. This will be handled internally.

First and foremost, the frontier will be empty and besides the start node. Here we create a dictionary keeping track of where we came from: “came_from” where each node is the key. Each location will point to the node that we came from (i.e came_from[B] == A. The same way we need a dictionary keeping track of the cost so far. The entries will again be the nodes that contains the cost from getting from the start node to the current node. The first While loop will get the next node in the frontier and expand the current node.

The next step is a for loop over neighbors, it will expand all neighboring nodes of the current node from frontier and calculate the cost so far as well as the heuristics from the next neighbor node to the end goal, i.e. the heuristic. First the “new_cost” will calculate the cost of the current node from the “cost_so_far(current)” and add the cost to getting to the next node by the provided function “get_cell_value(next)” in Map.py. 

If the neighboring nodes cost f(n) has not been calculated i.e not been expanded, or if the new cost is less than the previous i.e, it has been expanded but we now have found a new path, then the new cost is set for the the neighboring node and is added to the frontier as well as a new calculated heuristic. 


Neighbors “def neighbors”: 
    Gets the surrounding nodes around the current node and appends this to the result list. The function will also avoid adding not reachable nodes such as walls and not path-like nodes by doing an if check. Since the game does not allow for diagonal movements, it only has four edges to the four surrounding nodes around the current node. 


Heuristic function "def heuristic": 
    Takes inn to nodes and sets their x and y coordinates respectively to the to nodes, e.g "next" = the next node from the neighbours to the current node that we have expanded and "goal" = the end goal node, and uses this to predict the distance from the "next" node to the 


Show path “reconstruct_path”: 
    The function recreates the path by traversing the “came_from” dictionary. There will be no path if the goal doesn’t have a node that it came from.  


